﻿'Star Citizen Alternative Patch Download tool
'Created by Graeme Nimmo (NimmoG)
'Cloud Imperium Games own all Star Citizen related assets and code
'This tool queries the versions available to the official patcher and allows you to download the files for a release using http.

Imports System.IO

Public Class frmMainMenu
    Private urlList As New ArrayList

    Private versionList As New ArrayList

    Dim r As Random = New Random

    Private Structure universe
        Public versionName As String
        Public fileIndex As String
    End Structure

    'Begin downloading the files.
    Private Sub cmdDownloadFiles_Click(sender As Object, e As EventArgs) Handles cmdDownloadFiles.Click#
        Dim downloadLocation As String = txtSaveLocation.Text
        Dim fileNumber As Integer = 1
        Dim destination As String = ""

        'Check to see that we have some URLs and that a download location is set
        If urlList.Count > 0 And txtSaveLocation.Text <> "" Then
            'Try to catch a timeout exception
            Try
                'Download each file
                For Each file In urlList
                    lblProgress.Text = "Downloading file " & fileNumber & " of " & urlList.Count
                    destination = downloadLocation & getFileName(file)

                    'Make sure we're not overwriting the files. If statement used to prevent an exception from being thrown.
                    If Not My.Computer.FileSystem.FileExists(destination) Then '                v False flag for overwriting existing files. Throws an exception if you try.
                        My.Computer.Network.DownloadFile(file, destination, "", "", True, 1000, False)
                    End If

                    'Increment file count
                    fileNumber += 1
                Next

                'If user cancels the download delete the partially downloaded file to allow for simple resume support
            Catch ex As System.OperationCanceledException
                My.Computer.FileSystem.DeleteFile(destination)
            Catch ex As DirectoryNotFoundException
                MsgBox("There was a problem writing to that location, please check you have permission to write there.")
                'ToDo: Catch Not enough disk space exception
            Catch ex As Exception
                My.Computer.FileSystem.DeleteFile(destination)
                MsgBox("Something unexpected happened, please contact graeme@nimmog.co.uk and pass on a screenshot of this message: " & ex.ToString)
            End Try
        Else
            MsgBox("Please provide a download location before attempting to download files.")
        End If

        lblProgress.Text = fileNumber - 1 & " files downloaded."

        'If we've managed to download all of the files then delete the exported SC-URLs.txt file
        If fileNumber - 1 = urlList.Count Then
            My.Computer.FileSystem.DeleteFile("SC-URLs.txt")
        End If
    End Sub

    Private Function getFileName(ByVal url As String)
        Dim parts As String() = url.Split(New Char() {"/"c})
        Dim filename As String = ""
        For section = 5 To parts.Count - 1
            filename += "\" & parts(section)
        Next
        Return filename
    End Function

    Private Sub cmdSelectVersion_Click(sender As Object, e As EventArgs) Handles cmdSelectVersion.Click
        Dim requestedUniverse As String = cmbVersionsFound.SelectedItem
        Dim universeFileList As String = ""
        Dim prefix As String
        Dim fileList As New ArrayList


        ' File Handling Variables
        Dim fileName As String = "fileList.json"
        Dim baseURLs As New ArrayList

        'Need to make sure we actually have some universe selected.
        If requestedUniverse <> Nothing Then

            'I probably should make a neater way to select universe, but with a normally small list of universes the impact of this inefficient method is negligible
            For Each universe In versionList
                If requestedUniverse.Equals(universe.versionName) Then
                    universeFileList = universe.fileIndex
                ElseIf requestedUniverse = "Existing URL list" Then
                    universeFileList = "local"
                End If
            Next

            Try
                'Get the file list
                If Not universeFileList.Equals("") And Not universeFileList.Equals("local") Then
                    My.Computer.Network.DownloadFile(universeFileList, fileName, "", "", False, 1000, True)

                    Dim reader = File.OpenText(fileName)
                    Dim line As String
                    Dim randomBase As Integer
                    Dim writer As New StreamWriter("SC-URLs.txt", False)

                    'Read line from file
                    seekToLine(reader, "file_list")

                    line = reader.ReadLine()
                    Do
                        'Need to strip out the extra quotation marks to use.
                        fileList.Add(stripQuotations(line))
                        line = reader.ReadLine()
                    Loop Until line.Contains("],")

                    lblProgress.Text = fileList.Count & " files are ready to download."

                    'Find the Prefix
                    line = seekToLine(reader, "key_prefix")
                    Dim parts() As String = line.Split(Chr(34))
                    prefix = parts(3)

                    'Get Base URLs
                    seekToLine(reader, "webseed_urls")
                    line = reader.ReadLine()
                    Do
                        'Added support for downloading from multiple hosts in response to work network going into lockdown mode when pulling from first server
                        baseURLs.Add(stripQuotations(line))
                        line = reader.ReadLine()
                    Loop Until line.Contains("]")

                    'Generate complete URLs
                    For Each item In fileList
                        'In order to split load between different seed servers we randomly select from the base urls.
                        randomBase = randomNumber(0, (baseURLs.Count - 1))

                        urlList.Add(baseURLs(randomBase) & "/" & prefix & "/" & item)
                        writer.WriteLine(baseURLs(randomBase) & "/" & prefix & "/" & item)
                    Next

                    writer.Close()
                    reader.Close()
                    'Tidying up after ourselves

                    My.Computer.FileSystem.DeleteFile(fileName)
                ElseIf universeFileList.Equals("local") Then
                    lblProgress.Text = "Resuming from existing URL list."
                Else
                    MsgBox("There was an error finding the file list, did you select a version to download?")
                End If



                'Make the download button visible now that we've done the prep work.
                cmdDownloadFiles.Visible = True

            Catch ex As System.Net.WebException
                MsgBox("Sorry, the connection timed out, are you connected to the internet?")
            End Try
        Else
            MsgBox("Please select a version of Star Citizen to download.")
        End If
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lblADILink.LinkClicked
        Process.Start(lblADILink.Text)
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles imgADILogo.Click
        Process.Start(lblADILink.Text)
    End Sub

    Private Sub cmdBrowse_Click(sender As Object, e As EventArgs) Handles cmdBrowse.Click
        Dim folderDlg As New FolderBrowserDialog
        folderDlg.ShowNewFolderButton = True
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            txtSaveLocation.Text = folderDlg.SelectedPath
            Dim root As Environment.SpecialFolder = folderDlg.RootFolder
        End If

    End Sub

    Private Function seekToLine(ByRef file As System.IO.StreamReader, ByVal lineContents As String)
        Dim line As String
        Do
            line = file.ReadLine()
        Loop Until line.Contains(lineContents)
        Return line
    End Function

    Private Function stripQuotations(line As String) As Object
        Dim parts() As String = line.Split(Chr(34))
        Return parts(1)
    End Function

    Private Sub frmMainMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cmdDownloadFiles.Visible = False
        lblProgress.Text = ""
        txtSaveLocation.Text = Directory.GetCurrentDirectory & "\Star Citizen"
        'Clear the URL List
        urlList.Clear()

        If My.Computer.FileSystem.FileExists("SC-URLs.txt") Then
            readExistingFileList()
        End If
        prepareNewPatchList()

    End Sub

    Private Sub readExistingFileList()
        Dim reader = File.OpenText("SC-URLs.txt")
        Dim line As String
        Dim existingUniverse As New universe

        While reader.Peek <> -1
            line = reader.ReadLine
            urlList.Add(line)
        End While
        existingUniverse.versionName = "Existing URL list"
        existingUniverse.fileIndex = ""
        versionList.Add(existingUniverse)
        cmbVersionsFound.Items.Add(existingUniverse.versionName)
    End Sub

    Private Sub prepareNewPatchList()
        Dim fileLocation As String = "LauncherInfo.txt"
        Try
            My.Computer.Network.DownloadFile("http://manifest.robertsspaceindustries.com/Launcher/_LauncherInfo", fileLocation, "", "", False, 1000, True)

            'Load file to arrayList
            If My.Computer.FileSystem.FileExists(fileLocation) Then
                Dim reader = File.OpenText(fileLocation)
                Dim line As String
                Dim currentUniverse As universe
                Dim versionName As String
                Dim filePrefix As String
                Dim parts As String()
                cmbVersionsFound.Items.Clear()

                'Get available universes
                line = reader.ReadLine()
                parts = line.Split(New Char() {"="c})
                parts = parts(1).Split(New Char() {","c})
                For Each word In parts
                    versionName = word.Substring(1)

                    Do
                        line = reader.ReadLine()
                    Loop Until line.StartsWith(versionName & "_fileIndex")
                    parts = line.Split(New Char() {"="c})
                    filePrefix = parts(1).Substring(1)
                    currentUniverse.versionName = versionName
                    currentUniverse.fileIndex = filePrefix
                    versionList.Add(currentUniverse)
                Next

                For Each universe In versionList
                    cmbVersionsFound.Items.Add(universe.versionName)
                Next

                If cmbVersionsFound.Items.Count > 0 Then
                    cmbVersionsFound.SelectedIndex = 0
                End If
                reader.Close()
            End If

            My.Computer.FileSystem.DeleteFile(fileLocation)

        Catch ex As System.Net.WebException
            MsgBox("Sorry, the connection timed out, are you connected to the internet?")
        End Try
    End Sub


    Private Function randomNumber(ByVal min As Integer, ByVal max As Integer) As Integer

        Return r.Next(min, max)
    End Function


End Class