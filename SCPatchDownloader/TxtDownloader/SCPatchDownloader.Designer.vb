﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMainMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMainMenu))
        Me.lblDownloadFolder = New System.Windows.Forms.Label()
        Me.txtSaveLocation = New System.Windows.Forms.TextBox()
        Me.cmdDownloadFiles = New System.Windows.Forms.Button()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.cmbVersionsFound = New System.Windows.Forms.ComboBox()
        Me.cmdSelectVersion = New System.Windows.Forms.Button()
        Me.lblCredits = New System.Windows.Forms.Label()
        Me.imgADILogo = New System.Windows.Forms.PictureBox()
        Me.lblADILink = New System.Windows.Forms.LinkLabel()
        Me.cmdBrowse = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.lblInstruction = New System.Windows.Forms.Label()
        CType(Me.imgADILogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblDownloadFolder
        '
        Me.lblDownloadFolder.AutoSize = True
        Me.lblDownloadFolder.Enabled = False
        Me.lblDownloadFolder.Location = New System.Drawing.Point(27, 113)
        Me.lblDownloadFolder.Name = "lblDownloadFolder"
        Me.lblDownloadFolder.Size = New System.Drawing.Size(87, 13)
        Me.lblDownloadFolder.TabIndex = 5
        Me.lblDownloadFolder.Text = "Download Folder"
        '
        'txtSaveLocation
        '
        Me.txtSaveLocation.HideSelection = False
        Me.txtSaveLocation.Location = New System.Drawing.Point(120, 110)
        Me.txtSaveLocation.Name = "txtSaveLocation"
        Me.txtSaveLocation.Size = New System.Drawing.Size(236, 20)
        Me.txtSaveLocation.TabIndex = 4
        '
        'cmdDownloadFiles
        '
        Me.cmdDownloadFiles.Location = New System.Drawing.Point(120, 196)
        Me.cmdDownloadFiles.Name = "cmdDownloadFiles"
        Me.cmdDownloadFiles.Size = New System.Drawing.Size(104, 33)
        Me.cmdDownloadFiles.TabIndex = 3
        Me.cmdDownloadFiles.Text = "Download Files"
        Me.cmdDownloadFiles.UseVisualStyleBackColor = True
        '
        'lblProgress
        '
        Me.lblProgress.AutoSize = True
        Me.lblProgress.Location = New System.Drawing.Point(117, 155)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(16, 13)
        Me.lblProgress.TabIndex = 6
        Me.lblProgress.Text = "---"
        '
        'cmbVersionsFound
        '
        Me.cmbVersionsFound.FormattingEnabled = True
        Me.cmbVersionsFound.Location = New System.Drawing.Point(30, 55)
        Me.cmbVersionsFound.Name = "cmbVersionsFound"
        Me.cmbVersionsFound.Size = New System.Drawing.Size(121, 21)
        Me.cmbVersionsFound.TabIndex = 8
        '
        'cmdSelectVersion
        '
        Me.cmdSelectVersion.Location = New System.Drawing.Point(162, 45)
        Me.cmdSelectVersion.Name = "cmdSelectVersion"
        Me.cmdSelectVersion.Size = New System.Drawing.Size(104, 39)
        Me.cmdSelectVersion.TabIndex = 9
        Me.cmdSelectVersion.Text = "Select Release"
        Me.cmdSelectVersion.UseVisualStyleBackColor = True
        '
        'lblCredits
        '
        Me.lblCredits.AutoSize = True
        Me.lblCredits.Location = New System.Drawing.Point(17, 265)
        Me.lblCredits.Name = "lblCredits"
        Me.lblCredits.Size = New System.Drawing.Size(341, 13)
        Me.lblCredits.TabIndex = 10
        Me.lblCredits.Text = "SC Patch Downloader. Created by NimmoG of Atlas Defense Industries"
        '
        'imgADILogo
        '
        Me.imgADILogo.Image = CType(resources.GetObject("imgADILogo.Image"), System.Drawing.Image)
        Me.imgADILogo.Location = New System.Drawing.Point(451, 196)
        Me.imgADILogo.Name = "imgADILogo"
        Me.imgADILogo.Size = New System.Drawing.Size(71, 104)
        Me.imgADILogo.TabIndex = 11
        Me.imgADILogo.TabStop = False
        '
        'lblADILink
        '
        Me.lblADILink.AutoSize = True
        Me.lblADILink.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline
        Me.lblADILink.Location = New System.Drawing.Point(18, 282)
        Me.lblADILink.Name = "lblADILink"
        Me.lblADILink.Size = New System.Drawing.Size(165, 13)
        Me.lblADILink.TabIndex = 12
        Me.lblADILink.TabStop = True
        Me.lblADILink.Text = "www.AtlasDefenseIndustries.com"
        '
        'cmdBrowse
        '
        Me.cmdBrowse.Location = New System.Drawing.Point(362, 110)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(28, 20)
        Me.cmdBrowse.TabIndex = 13
        Me.cmdBrowse.Text = "..."
        Me.cmdBrowse.UseVisualStyleBackColor = True
        '
        'lblInstruction
        '
        Me.lblInstruction.AutoSize = True
        Me.lblInstruction.Location = New System.Drawing.Point(14, 12)
        Me.lblInstruction.Name = "lblInstruction"
        Me.lblInstruction.Size = New System.Drawing.Size(295, 13)
        Me.lblInstruction.TabIndex = 14
        Me.lblInstruction.Text = "Please select a release and the Download button will appear."
        '
        'frmMainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(529, 343)
        Me.Controls.Add(Me.lblInstruction)
        Me.Controls.Add(Me.cmdBrowse)
        Me.Controls.Add(Me.lblADILink)
        Me.Controls.Add(Me.imgADILogo)
        Me.Controls.Add(Me.lblCredits)
        Me.Controls.Add(Me.cmdSelectVersion)
        Me.Controls.Add(Me.cmbVersionsFound)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.lblDownloadFolder)
        Me.Controls.Add(Me.txtSaveLocation)
        Me.Controls.Add(Me.cmdDownloadFiles)
        Me.Name = "frmMainMenu"
        Me.Text = "Star Citizen Alternative Patch Downloader"
        CType(Me.imgADILogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblDownloadFolder As System.Windows.Forms.Label
    Friend WithEvents txtSaveLocation As System.Windows.Forms.TextBox
    Friend WithEvents cmdDownloadFiles As System.Windows.Forms.Button
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents cmbVersionsFound As System.Windows.Forms.ComboBox
    Friend WithEvents cmdSelectVersion As System.Windows.Forms.Button
    Friend WithEvents lblCredits As System.Windows.Forms.Label
    Friend WithEvents imgADILogo As System.Windows.Forms.PictureBox
    Friend WithEvents lblADILink As System.Windows.Forms.LinkLabel
    Friend WithEvents cmdBrowse As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents lblInstruction As System.Windows.Forms.Label
End Class
